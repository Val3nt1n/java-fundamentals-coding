package Ex4;

//Enter two values of type int. Display their division casted to the double type and rounded to
//the third decimal point.
public class Ex4 {
    public static void main(String[] args) {
        int a, b;
        a = new Integer(25);
        b = new Integer(2);
        double c = (double) a/b;
        System.out.printf("Value : %.3f", c);
    }
}

