package Ex2;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Give me a double: ");
        double v = keyboard.nextDouble();
        System.out.println("v = " + v);

        System.out.printf("\nValue as an int: %14d", Math.round(v));
        System.out.printf("\nValue as an int: %o", Math.round(v));
        System.out.printf("\nValue as an int: %2.4f", v);

        System.out.printf("\nAll on one line: %d %o %.2f", Math.round(v), Math.round(v), v);
    }
}

