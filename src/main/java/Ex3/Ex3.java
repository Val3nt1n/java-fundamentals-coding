package Ex3;

// Display any three strings of characters on one line so that they are aligned to the right
// edge of the 15-character blocks. How to align strings to the left edge?
public class Ex3 {
    public static void main(String[] args) {
        String a = "Primul";
        String b = "ajskdha";
        String c = "lkjgdsa";
        System.out.printf("%15s %n %15s %n %-15s", a, b, c);
    }
}
