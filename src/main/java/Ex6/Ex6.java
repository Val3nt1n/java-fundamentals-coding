package Ex6;

import java.util.Scanner;

//Create three variables, one for each type: float, byte and char. Enter values corresponding to
//those types using Scanner. What values are you able to enter for each type?

public class Ex6 {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Scrie o valoare float : ");
        float f = keyboard.nextFloat();
        System.out.println("\nValoarea float este : " + f);
        System.out.print("\nScrie o valoare byte : ");
        byte b = keyboard.nextByte();
        System.out.println("\nValoarea byte este : " + b);
        System.out.print("\nScrie o valoare char : ");
        char c = keyboard.next().charAt(0);
        System.out.println("\nValoarea char este : " + c);
    }
}
